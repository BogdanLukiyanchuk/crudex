package com.bogluk.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bogluk.model.User;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private final SortingOrder order = new SortingOrder();
    private final FilterResults filter = new FilterResults();

    @Override
    public void addUser(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    @Override
    public void deleteUser(Integer userId) {
        User user = (User) sessionFactory.getCurrentSession().load(User.class, userId);
        if (null != user) {
            this.sessionFactory.getCurrentSession().delete(user);
        }
    }

    @Override
    public User getUser(int userId) {
        return (User) sessionFactory.getCurrentSession().get(User.class, userId);
    }

    @Override
    public User updateUser(User user) {
        sessionFactory.getCurrentSession().update(user);
        return user;
    }


    private static Criteria getCriteria(final Session session, FilterResults filter, SortingOrder order) {
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.isNotNull("name"));
        if (!FilterResults.DEFAULT.equals(filter)) {
            final FilterResults DEFAULT = FilterResults.DEFAULT;
            if (DEFAULT.id != filter.id) {
                criteria.add(Restrictions.eq("id", filter.id));
            }
            if (DEFAULT.exactMatch != filter.exactMatch) {
                if (filter.exactMatch) {
                    criteria.add(Restrictions.eq("name", filter.partOfName));
                } else if (!DEFAULT.partOfName.equals(filter.partOfName)) {
                    criteria.add(Restrictions.like("name", filter.partOfName, MatchMode.ANYWHERE));
                }
            }
            if (DEFAULT.minAge != filter.minAge) {
                criteria.add(Restrictions.ge("age", filter.minAge));
            }
            if (DEFAULT.maxAge != filter.maxAge) {
                criteria.add(Restrictions.le("age", filter.maxAge));
            }
            if (DEFAULT.admin != filter.admin) {
                criteria.add(Restrictions.eq("admin", filter.admin));
            }
        }

        for (Map.Entry<String, Boolean> entry : order.entrySet()) {
            if (entry.getValue()) {
                criteria.addOrder(Order.desc(entry.getKey()));
            } else {
                criteria.addOrder(Order.asc(entry.getKey()));
            }
        }
        return criteria;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getUsersUsingCriteria(int pageNumber, int numberOfResults) {
        final Session session = sessionFactory.getCurrentSession();
        Criteria criteria = getCriteria(session, filter, order);
        criteria.setFirstResult(pageNumber * numberOfResults);
        criteria.setMaxResults(numberOfResults);
        return criteria.list();
    }

    @Override
    public int getTotalPages(int usersOnPage) {
        final Session session = sessionFactory.getCurrentSession();
        Criteria criteria = getCriteria(session, filter, order);
        ScrollableResults scrollableResults = criteria.scroll();
        int usersCount = scrollableResults.last()
                ? scrollableResults.getRowNumber() + 1
                : 0;
        scrollableResults.close();

        return usersCount == 0 ? 0 : (usersCount - 1) / usersOnPage + 1;
    }

    @Override
    public void updateSortingOrder(String propertyName, String direction) {
        order.up(propertyName, direction);
    }

    @Override
    public FilterResults getFilter() {
        return filter;
    }

}
