package com.bogluk.controller;

import com.bogluk.dao.FilterResults;
import com.bogluk.service.UserService;

public class SearchForm {
    String id = "";
    Integer searchOnName = 0;
    String partOfName = "";
    String minAge = "";
    String maxAge = "";
    String searchOnAdmin = "any";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSearchOnName() {
        return searchOnName;
    }

    public void setSearchOnName(Integer searchOnName) {
        this.searchOnName = searchOnName;
    }

    public String getPartOfName() {
        return partOfName;
    }

    public void setPartOfName(String partOfName) {
        this.partOfName = partOfName;
    }

    public String getMinAge() {
        return minAge;
    }

    public void setMinAge(String minAge) {
        this.minAge = minAge;
    }

    public String getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(String maxAge) {
        this.maxAge = maxAge;
    }

    public String getSearchOnAdmin() {
        return searchOnAdmin;
    }

    public void setSearchOnAdmin(String searchOnAdmin) {
        this.searchOnAdmin = searchOnAdmin;
    }

    void load(UserService userService) {
        FilterResults filter = userService.getFilter();
        if (FilterResults.DEFAULT.getId() == filter.getId()) {
            id = "";
        } else {
            id = String.valueOf(filter.getId());
        }
        searchOnName = filter.getExactMatch() == null ? 0 : !filter.getExactMatch() ? 1 : 2;
        partOfName = filter.getPartOfName();
        if (FilterResults.DEFAULT.getMinAge() == filter.getMinAge()) {
            minAge = "";
        } else {
            minAge = String.valueOf(filter.getMinAge());
        }
        if (FilterResults.DEFAULT.getMaxAge() == filter.getMaxAge()) {
            maxAge = "";
        } else {
            maxAge = String.valueOf(filter.getMaxAge());
        }
        searchOnAdmin = filter.getAdmin() == null ? "any" : filter.getAdmin() ? "yes" : "no";

    }
}
