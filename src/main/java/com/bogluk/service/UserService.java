package com.bogluk.service;

import com.bogluk.dao.FilterResults;
import java.util.List;

import com.bogluk.model.User;

public interface UserService {

    public void addUser(User user);

    public void deleteUser(Integer userId);

    public User getUser(int userId);

    public User updateUser(User user);

    public List<User> getUsersUsingCriteria(int pageNumber, int numberOfResults);

    public Integer getTotalPages(int usersOnPage);
    
    public void updateSortingOrder(String propertyName, String direction);
    
    public FilterResults getFilter();

}
