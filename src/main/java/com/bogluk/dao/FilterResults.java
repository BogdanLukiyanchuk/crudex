package com.bogluk.dao;

public class FilterResults {
    int id = 0;
    Boolean exactMatch = null;
    String partOfName = "";
    int minAge = 0;
    int maxAge = 200;
    Boolean admin = null;
    
    public final static FilterResults DEFAULT = new FilterResults();
    
    public void clear() {
        id = DEFAULT.id;
        exactMatch = DEFAULT.exactMatch;
        partOfName = DEFAULT.partOfName;
        minAge = DEFAULT.minAge;
        maxAge = DEFAULT.maxAge;
        admin = DEFAULT.admin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getExactMatch() {
        return exactMatch;
    }

    public void setExactMatch(Boolean exactMatch) {
        this.exactMatch = exactMatch;
    }

    public String getPartOfName() {
        return partOfName;
    }

    public void setPartOfName(String partOfName) {
        this.partOfName = partOfName;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FilterResults that = (FilterResults) o;

        if (id != that.id) return false;
        if (minAge != that.minAge) return false;
        if (maxAge != that.maxAge) return false;
        if (exactMatch != null ? !exactMatch.equals(that.exactMatch) : that.exactMatch != null) return false;
        if (partOfName != null ? !partOfName.equals(that.partOfName) : that.partOfName != null) return false;
        return admin != null ? admin.equals(that.admin) : that.admin == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (exactMatch != null ? exactMatch.hashCode() : 0);
        result = 31 * result + (partOfName != null ? partOfName.hashCode() : 0);
        result = 31 * result + minAge;
        result = 31 * result + maxAge;
        result = 31 * result + (admin != null ? admin.hashCode() : 0);
        return result;
    }
}
