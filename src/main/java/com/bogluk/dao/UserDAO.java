package com.bogluk.dao;

import java.util.List;
import com.bogluk.model.User;

public interface UserDAO {

    public void addUser(User user);

    public void deleteUser(Integer userId);

    public User updateUser(User user);

    public User getUser(int userId);

    public List<User> getUsersUsingCriteria(int pageNumber, int numberOfResults);

    public int getTotalPages(int usersOnPage);
    
    public void updateSortingOrder(String propertyName, String direction);

    public FilterResults getFilter();

}
