package com.bogluk.controller;

import com.bogluk.dao.FilterResults;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.bogluk.model.User;
import com.bogluk.service.UserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

    private static final Logger LOGGER = Logger
            .getLogger(UserController.class);
    private String lastPage = "/";
    private Integer usersOnPage = 20;
    private SearchForm searchForm = new SearchForm();

    public UserController() {
        System.out.println("UserController()");
    }

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/")
    public ModelAndView listUser(ModelAndView model) throws IOException {
        return new ModelAndView("redirect:/pages/0");
    }

    @RequestMapping(value = "/pages/{pageNumber}", params = "!sort", method = RequestMethod.GET)
    public ModelAndView listUser(@PathVariable Integer pageNumber,
            ModelAndView model) throws IOException {
        List<User> listUser = userService.getUsersUsingCriteria(pageNumber, usersOnPage);
        Integer totalPages = userService.getTotalPages(usersOnPage);
        model.addObject("listUser", listUser);
        model.addObject("totalPages", totalPages);
        model.addObject("currentIndex", pageNumber);
        model.addObject("usersOnPage", usersOnPage);
        searchForm.load(userService);
        model.addObject("form", searchForm);
        model.setViewName("pages");
        lastPage = "/pages/" + pageNumber;
        return model;
    }

    @RequestMapping(value = "/pages/{pageNumber}", method = RequestMethod.GET)
    public ModelAndView updateSortingOrder(@PathVariable Integer pageNumber,
            @RequestParam("sort") String propertyName, @RequestParam("direction") String direction,
            ModelAndView model) throws IOException {
        userService.updateSortingOrder(propertyName, direction);
        return listUser(pageNumber, model);
    }

    @RequestMapping(value = "/usersOnPage/{number}", method = RequestMethod.GET)
    public ModelAndView setNumberOfUsersOnPage(@PathVariable Integer number,
            ModelAndView model) throws IOException {
        usersOnPage = (number > 0 && number <= 1000) ? number : 20;
        return new ModelAndView("redirect:/pages/0");
    }

    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
    public ModelAndView newContact(ModelAndView model) {
        User user = new User();
        model.addObject("user", user);
        model.setViewName("UserForm");
        return model;
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ModelAndView saveUser(@ModelAttribute User user) {
        if (user.getId() == 0) {
            // if user id is 0 then creating the
            // user other updating the user
            userService.addUser(user);
        } else {
            userService.updateUser(user);
        }
        return new ModelAndView("redirect:" + lastPage);
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public ModelAndView deleteUser(HttpServletRequest request) {
        int userId = Integer.parseInt(request.getParameter("id"));
        userService.deleteUser(userId);
        return new ModelAndView("redirect:" + lastPage);
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public ModelAndView editContact(HttpServletRequest request) {
        int userId = Integer.parseInt(request.getParameter("id"));
        User user = userService.getUser(userId);
        ModelAndView model = new ModelAndView("UserForm");
        model.addObject("user", user);

        return model;
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public ModelAndView findUsers(@ModelAttribute SearchForm form) {
        FilterResults filter = userService.getFilter();
        filter.clear();
        try {
            if (!form.id.isEmpty()) {
                filter.setId(Integer.parseInt(form.id));
            }
            filter.setExactMatch(form.searchOnName == 0 ? null : form.searchOnName == 2);
            filter.setPartOfName(form.partOfName);
            if (!form.minAge.isEmpty()) {
                filter.setMinAge(Integer.parseInt(form.minAge));
            }
            if (!form.maxAge.isEmpty()) {
                filter.setMaxAge(Integer.parseInt(form.maxAge));
            }
            filter.setAdmin(form.searchOnAdmin.equals("any") ? null : form.searchOnAdmin.equals("yes"));
        } catch(NumberFormatException e) {
            //do nothing
        }
        return new ModelAndView("redirect:/pages/0");
    }

    @RequestMapping(value = "/clear", method = RequestMethod.GET)
    public ModelAndView clearSearchForm(ModelAndView model) {
        FilterResults filter = userService.getFilter();
        filter.clear();
        return new ModelAndView("redirect:/pages/0");
    }

}
