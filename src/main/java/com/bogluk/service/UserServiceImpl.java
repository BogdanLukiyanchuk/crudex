package com.bogluk.service;

import com.bogluk.dao.FilterResults;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogluk.model.User;
import com.bogluk.dao.UserDAO;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public void addUser(User user) {
        userDAO.addUser(user);
    }

    @Override
    @Transactional
    public void deleteUser(Integer userId) {
        userDAO.deleteUser(userId);
    }

    @Override
    public User getUser(int userId) {
        return userDAO.getUser(userId);
    }

    @Override
    public User updateUser(User user) {
        return userDAO.updateUser(user);
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    @Transactional
    public List<User> getUsersUsingCriteria(int pageNumber, int numberOfResults) {
        return userDAO.getUsersUsingCriteria(pageNumber, numberOfResults);
    }

    @Override
    @Transactional
    public Integer getTotalPages(int usersOnPage) {
        return userDAO.getTotalPages(usersOnPage);
    }

    @Override
    public void updateSortingOrder(String propertyName, String direction) {
        userDAO.updateSortingOrder(propertyName, direction);
    }

    @Override
    public FilterResults getFilter() {
        return userDAO.getFilter();
    }

    
}
