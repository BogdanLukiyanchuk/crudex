package com.bogluk.dao;

import java.util.LinkedHashMap;

public class SortingOrder extends LinkedHashMap<String, Boolean>{

    public SortingOrder() {
        this.put("id", Boolean.FALSE);
        this.put("admin", Boolean.TRUE);
        this.put("age", Boolean.TRUE);
        this.put("name", Boolean.FALSE);
        this.put("createdDate", Boolean.FALSE);
    }
    
    public void up(String upped, String order) {
        if (null != this.remove(upped)) {
            SortingOrder copy = (SortingOrder) this.clone();
            Boolean value = order.equals("desc");
            this.clear();
            this.put(upped, value);
            this.putAll(copy);
        }
    }
}
