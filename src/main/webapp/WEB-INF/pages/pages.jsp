<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="firstUrl" value="/pages/0"/>
<c:url var="lastUrl" value="/pages/${totalPages - 1}"/>
<c:url var="prevUrl" value="/pages/${currentIndex - 1}"/>
<c:url var="nextUrl" value="/pages/${currentIndex + 1}"/>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User Management Screen</title>
</head>
<body>
<div align="center">
    <h1>User List</h1>
    <table>
        <th></th>
        <th></th>
        <tr>
            <td align="left" style="vertical-align: top">
                <form:form action="/find" method="get" modelAttribute="form">
                    <ul>
                        <li>Id</li>
                        <form:input path="id" size="4"/><br>
                        <br>

                        <li>Name</li>
                        <form:radiobutton path="searchOnName" value="0" label="no filter"/><br>
                        <form:radiobutton path="searchOnName" value="1" label="contains"/><br>
                        <form:radiobutton path="searchOnName" value="2" label="exact"/><br>
                        <form:input path="partOfName" size="14"/><br>
                        <br>

                        <li>Age between</li>
                        <form:input path="minAge" size="3"/><br>
                        <form:input path="maxAge" size="3"/><br>
                        <br>

                        <li>Admin</li>
                        <form:radiobutton path="searchOnAdmin" value="any" label="any"/><br>
                        <form:radiobutton path="searchOnAdmin" value="yes" label="yes"/><br>
                        <form:radiobutton path="searchOnAdmin" value="no" label="no"/><br>
                    </ul>
                    <div align="center">
                        <input type="submit" value="Find" style="width: 100px">
                    </div>
                    <br>
                </form:form>
                <div align="center">
                    <form action="<c:url value='/clear'/>">
                        <input type="submit" value="Clear" style="width: 100px">
                    </form>
                </div>
            </td>
            <td align="center" style="vertical-align: top">

                <div class="pagination">
                    <c:choose>
                        <c:when test="${currentIndex == 0}">
                            <a href="#">&lt;&lt;</a>
                            &nbsp;&nbsp;
                            <a href="#">&lt;</a>
                        </c:when>
                        <c:otherwise>
                            <a href="${firstUrl}">&lt;&lt;</a>
                            &nbsp;&nbsp;
                            <a href="${prevUrl}">&lt;</a>
                        </c:otherwise>
                    </c:choose>
                    &nbsp;&nbsp;
                    Page ${currentIndex + 1} of ${totalPages}
                    &nbsp;&nbsp;
                    <c:choose>
                        <c:when test="${currentIndex == totalPages - 1}">
                            <a href="#">&gt;</a>
                            &nbsp;&nbsp;
                            <a href="#">&gt;&gt;</a>
                        </c:when>
                        <c:otherwise>
                            <a href="${nextUrl}">&gt;</a>
                            &nbsp;&nbsp;
                            <a href="${lastUrl}">&gt;&gt;</a>
                        </c:otherwise>
                    </c:choose>
                </div>

                <table border="1">
                    <th><a href="<c:url value='/pages/0?sort=id&direction=asc'/>"><img
                            src="<c:url value='/resources/images/sortup.png'/>" width="24" height="16"/></a>
                        Id
                        <a href="<c:url value='/pages/0?sort=id&direction=desc'/>">
                            <img src="<c:url value='/resources/images/sortdown.png'/>" width="24" height="16"/>
                        </a>
                    </th>
                    <th><a href="<c:url value='/pages/0?sort=name&direction=asc'/>"><img
                            src="<c:url value='/resources/images/sortup.png'/>" width="24" height="16"/></a>
                        Name
                        <a href="<c:url value='/pages/0?sort=name&direction=desc'/>">
                            <img src="<c:url value='/resources/images/sortdown.png'/>" width="24" height="16"/>
                        </a>
                    </th>
                    <th><a href="<c:url value='/pages/0?sort=age&direction=asc'/>"><img
                            src="<c:url value='/resources/images/sortup.png'/>" width="24" height="16"/></a>
                        Age
                        <a href="<c:url value='/pages/0?sort=age&direction=desc'/>">
                            <img src="<c:url value='/resources/images/sortdown.png'/>" width="24" height="16"/>
                        </a>
                    </th>
                    <th><a href="<c:url value='/pages/0?sort=admin&direction=asc'/>"><img
                            src="<c:url value='/resources/images/sortup.png'/>" width="24" height="16"/></a>
                        Admin
                        <a href="<c:url value='/pages/0?sort=admin&direction=desc'/>">
                            <img src="<c:url value='/resources/images/sortdown.png'/>" width="24" height="16"/>
                        </a>
                    </th>
                    <th><a href="<c:url value='/pages/0?sort=createdDate&direction=asc'/>"><img
                            src="<c:url value='/resources/images/sortup.png'/>" width="24" height="16"/></a>
                        Created
                        <a href="<c:url value='/pages/0?sort=createdDate&direction=desc'/>">
                            <img src="<c:url value='/resources/images/sortdown.png'/>" width="24" height="16"/>
                        </a>
                    </th>
                    <th>Action</th>

                    <c:forEach var="user" items="${listUser}">
                        <tr>
                            <td style="color: gray">${user.id}</td>
                            <td>${user.name}</td>
                            <td>${user.age}</td>
                            <td>${user.admin ? "yes" : "no"}</td>
                            <td>${user.createdDate}</td>
                            <td>
                                <a href="<c:url value='/editUser?id=${user.id}'/>">Edit</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="<c:url value='/deleteUser?id=${user.id}'/>">Delete</a>
                            </td>

                        </tr>
                    </c:forEach>
                </table>
                <div>
                    <br>
                    Set number of users on page
                    <c:set var="numbers" value="${[20, 50, 100]}"/>
                    <c:forEach var="i" begin="0" end="2">
                        <c:choose>
                            <c:when test="${numbers[i] == usersOnPage}">
                                &nbsp;&nbsp;${numbers[i]}
                            </c:when>
                            <c:otherwise>
                                &nbsp;&nbsp;<a href="<c:url value='/usersOnPage/${numbers[i]}'/>">${numbers[i]}</a>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </div>
                <h4>
                    Register New User <a href="<c:url value='/newUser'/>">here</a>
                </h4>
            </td>
        </tr>
    </table>
</div>

</body>
</html>