<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New/Edit User</title>
    </head>
    <body>
        <div align="center">
            <form:form action="saveUser" method="post" modelAttribute="user">
                <c:choose>
                    <c:when test="${user.id == 0}">
                        <h1>New User</h1>
                    </c:when>
                    <c:otherwise>
                        <h1>Edit User</h1>
                    </c:otherwise>
                </c:choose>
                <table>
                    <c:choose>
                        <c:when test="${user.id != 0}">
                            <tr>
                                <td>Id:</td>
                                <td><form:input path="id" readonly="true" /></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td>Id:</td>
                                <td><i>id will be set automatically</i></td>
                                <form:hidden path="id"/>
                            </tr>
                        </c:otherwise>
                    </c:choose>                
                    <tr>
                        <td>Name:</td>
                        <td><form:input path="name" /></td>
                    </tr>
                    <tr>
                        <td>Age:</td>
                        <td><form:input path="age" /></td>
                    </tr>
                    <tr>
                        <td>Admin:</td>
                        <td><form:checkbox path="admin" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="submit" value="Save"></td>
                    </tr>
                </table>
            </form:form>
        </div>
    </body>
</html>